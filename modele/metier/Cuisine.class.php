<?php
namespace modele\metier;
use modele\metier\Resto;

/**
 * Description of Cuisine
 *
 * @author E.Poirier
 * @version 07/2021
 */
class Cuisine {
    
    /** @var int IdTC , id du type de la cuisine */
    private ?int $idTC;
    /** @var string Nom du type de cuisine */
    private ?string $libelleTC;
    
    function __construct(?int $idTC, ?string $libelleTC) {
        $this->idTC = $idTC;
        $this->libelleTC = $libelleTC;
        }
    function getIdTC(): ?int {
        return $this->idTC;
    }
    function getLibelleTC(): ?string {
        return $this->libelleTC;
    }
    function setIdTC(?int $idTC): void {
        $this->idTC = $idTC;
    }
    function setLibelleTC(?string $libelleTC): void {
        $this->libelleTC = $libelleTC;
    }
        
         
}